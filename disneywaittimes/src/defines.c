#include "../../disneybase/constants.h"
#include <pebble.h>

const int color_hex = 0x00FFAA;

const int num_parks = 4;
char park_names[MAX_NUM_PARKS][40] = {"Magic Kingdom", "Hollywood Studios", "Epcot", "Animal Kingdom"};

int park_icon_ids[MAX_NUM_PARKS] = { RESOURCE_ID_ICON_MK, RESOURCE_ID_ICON_HS, RESOURCE_ID_ICON_EPCOT, RESOURCE_ID_ICON_AK };
