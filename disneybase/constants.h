#pragma once
  
#define DEBUG 1
  
#define WATCHED_DATA_KEY 0
#define WATCHED_COUNT_KEY 1

#define KEY_TYPE_UPDATE_COMPLETE 5
#define KEY_TYPE_RIDE_INFO 8
#define KEY_TYPE_PARK_HOURS 9

#define KEY_RIDE_ID 0
#define KEY_RIDE_NAME 1
#define KEY_RIDE_STATUS 2
#define KEY_RIDE_POSTED 3
#define KEY_PARK_INDEX 4
#define KEY_PARK_HOURS 7

#define MAX_NUM_PARKS 4
#define MAX_WATCHED_RIDES 20
#define MAX_ATTRACTIONS 50
#define MAX_WATCHED_RIDES 20

extern const int num_parks;
extern char park_names[MAX_NUM_PARKS][40];
extern int park_icon_ids[MAX_NUM_PARKS];

extern const int color_hex;

