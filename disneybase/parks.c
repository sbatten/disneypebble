#include <pebble.h>
#include "parks.h"
#include "data.h"
#include "constants.h"
#include "attractions.h"
  
#define NUM_PARK_MENU_SECTIONS 1
  
static struct ParksUi {
  Window *window;
  MenuLayer *list_layer;
  GBitmap *icons[MAX_NUM_PARKS];
  
  #ifdef PBL_SDK_3
    StatusBarLayer *status_bar;
  #endif
} ui;

// A callback is used to specify the amount of sections of menu items
// With this, you can dynamically add and remove sections
static uint16_t get_num_sections_callback(MenuLayer *menu_layer, void *data) {
  return NUM_PARK_MENU_SECTIONS;
}

// Each section has a number of items;  we use a callback to specify this
// You can also dynamically add and remove items using this
static uint16_t get_num_rows_callback(MenuLayer *menu_layer, uint16_t section_index, void *data) {
  return num_parks;
}

// A callback is used to specify the height of the section header
// static int16_t get_header_height_callback(MenuLayer *menu_layer, uint16_t section_index, void *data) {
//   // This is a define provided in pebble.h that you may use for the default height
//   return MENU_CELL_BASIC_HEADER_HEIGHT;
// }

// Here we draw what each header is
// static void draw_header_callback(GContext* ctx, const Layer *cell_layer, uint16_t section_index, void *data) {
//   menu_cell_basic_header_draw(ctx, cell_layer, "Park Info");
// }

// This is the menu item draw callback where you specify what each item should look like
static void draw_row_callback(GContext* ctx, const Layer *cell_layer, MenuIndex *cell_index, void *data) {
  
  // Use the row to specify which item we'll draw
  int row = cell_index->row;
  
  menu_cell_basic_draw(ctx, cell_layer, park_names[row], parks[row].valid ? parks[row].hours : NULL, ui.icons[row]);
}

static void select_callback(MenuLayer *menu_layer, MenuIndex *cell_index, void *data) {
  attractions_show(cell_index->row);
}

static void long_select_callback(MenuLayer *menu_layer, MenuIndex *cell_index, void *data) {
  vibes_short_pulse();
  request_new_data();
}

static void window_load(Window *window) {

  int i;  
  for(i = 0; i < num_parks; i++)
    ui.icons[i] = gbitmap_create_with_resource(park_icon_ids[i]);
  
  GRect menu_frame = layer_get_frame(window_get_root_layer(window));
  #ifdef PBL_SDK_3
    menu_frame.origin.y += STATUS_BAR_LAYER_HEIGHT;
    menu_frame.size.h -= STATUS_BAR_LAYER_HEIGHT;
  #endif
  ui.list_layer = menu_layer_create(menu_frame);
  
  #ifndef PBL_BW
    menu_layer_set_highlight_colors(ui.list_layer, GColorFromHEX(color_hex), GColorBlack);
    menu_layer_set_normal_colors(ui.list_layer, GColorWhite, GColorBlack);
    menu_layer_set_center_focused(ui.list_layer, false);
  #endif
  
  // Set all the callbacks for the menu layer
  menu_layer_set_callbacks(ui.list_layer, NULL, (MenuLayerCallbacks){
    .get_num_sections = get_num_sections_callback,
    .get_num_rows = get_num_rows_callback,
//    .get_header_height = get_header_height_callback,
//    .draw_header = draw_header_callback,
    .draw_row = draw_row_callback,
    .select_click = select_callback,
    .select_long_click = long_select_callback
  });
  
  // Bind the menu layer's click config provider to the window for interactivity
  menu_layer_set_click_config_onto_window(ui.list_layer, window);
  
  // Add it to the window for display
  layer_add_child(window_get_root_layer(window), menu_layer_get_layer(ui.list_layer));
  
  // Setup status bar
  #ifdef PBL_SDK_3
    ui.status_bar = status_bar_layer_create();
    status_bar_layer_set_colors(ui.status_bar, GColorFromHEX(color_hex), GColorBlack);
    layer_add_child(window_get_root_layer(window), status_bar_layer_get_layer(ui.status_bar));
  #endif
}

static void window_unload(Window *window) {
  menu_layer_destroy(ui.list_layer);
  int i;
  for(i = 0; i < num_parks; i++)
    gbitmap_destroy(ui.icons[i]);
  
  #ifdef PBL_SDK_3
    status_bar_layer_destroy(ui.status_bar);
  #endif
}


void parks_init(void) { 
  ui.window = window_create();
  
  window_set_window_handlers(ui.window, (WindowHandlers){
    .load = window_load,
    .unload = window_unload
  });
  
  window_stack_push(ui.window, true);
}

void parks_deinit(void) {
  window_destroy(ui.window);
}

void parks_refresh(void) {
  menu_layer_reload_data(ui.list_layer);
  layer_mark_dirty(menu_layer_get_layer(ui.list_layer));
}

