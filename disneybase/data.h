#pragma once

#include <pebble.h>
#include "constants.h"
  
typedef struct Attraction {
  int id, posted, watched, flagged;
  char name[40];
  char status[25];
} Attraction;

typedef struct Park {
  bool valid;
  char hours[10];
} Park;

time_t last_update_time;
int watched_count;
Park parks[MAX_NUM_PARKS];
Attraction attractions[MAX_NUM_PARKS][MAX_ATTRACTIONS];

int stored_watched_count;
int32_t stored_watched_ids[MAX_WATCHED_RIDES];

int get_attractions_count(int park_index);
int get_flagged_attractions_count();

void data_init(void);
void data_deinit(void);
void request_new_data(void);
