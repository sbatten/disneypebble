#include <pebble.h>
#include "constants.h"
#include "parks.h"
#include "update.h"
#include "attractions.h"
#include "data.h"

static int first_update_flag = 1;
static void tick_handler(struct tm *tick_time, TimeUnits units_changed) {
  // Get new data every 15 minutes
  time_t now = time(NULL);

  if(first_update_flag && first_update_flag++ == 2) {
    first_update_flag = 0;
    tick_timer_service_subscribe(MINUTE_UNIT, tick_handler);
  }
  else if(!first_update_flag && difftime(now, last_update_time) >= 900) {
    request_new_data();
  }
}

static int waits_update_count = 0;
static int hours_update_count = 0;

static void inbox_received_callback(DictionaryIterator *iterator, void *context) {
  static char ride_name_buffer[75], ride_status_buffer[20], park_hours_buffer[10];
  static int ride_posted_time, ride_id, park_index = 0;
  
  int type = -1;
  
  // Read first item
  Tuple *t = dict_read_first(iterator);

  // For all items
  while(t != NULL) {
    // Which key was received?
    switch(t->key) {
    case KEY_TYPE_UPDATE_COMPLETE:
      type = KEY_TYPE_UPDATE_COMPLETE;
      park_index = t->value->int32;
      break;
    case KEY_TYPE_RIDE_INFO:
      type = KEY_TYPE_RIDE_INFO;
      break;
    case KEY_TYPE_PARK_HOURS:
      type = KEY_TYPE_PARK_HOURS;
      break;
    case KEY_RIDE_ID:
      ride_id = t->value->int32;
      break;
    case KEY_RIDE_NAME:
      snprintf(ride_name_buffer, sizeof(ride_name_buffer), "%s", t->value->cstring);
      break;
    case KEY_RIDE_POSTED:
      ride_posted_time = t->value->int32;
      break;
    case KEY_RIDE_STATUS:
      snprintf(ride_status_buffer, sizeof(ride_status_buffer), "%s", t->value->cstring);
      break;
    case KEY_PARK_INDEX:  
      park_index = t->value->int32;
      break;
    
    case KEY_PARK_HOURS:
        snprintf(park_hours_buffer, sizeof(park_hours_buffer), "%s", t->value->cstring);
      break;
    default:
      APP_LOG(APP_LOG_LEVEL_ERROR, "Key %d not recognized!", (int)t->key);
      break;
    }

    // Look for next item
    t = dict_read_next(iterator);
  }
  
  switch(type) {
  case KEY_TYPE_RIDE_INFO: {
    APP_LOG(APP_LOG_LEVEL_INFO, "Updating ride");
    int i;
    for(i = 0; i < MAX_ATTRACTIONS; i++) {
      if(attractions[park_index][i].id == ride_id) {
        if(attractions[park_index][i].watched) {
          if(ride_posted_time != -1) {
            if(attractions[park_index][i].posted > ride_posted_time || attractions[park_index][i].posted == -1)
              attractions[park_index][i].flagged = 1;
          }
        }
        
        attractions[park_index][i].posted = ride_posted_time;
        strncpy(attractions[park_index][i].status, ride_status_buffer, 20);
        break;
      }
    }
    
    if(i == MAX_ATTRACTIONS) {
      for(i = 0; i < MAX_ATTRACTIONS; i++) {
        if(attractions[park_index][i].id == -1) {
          attractions[park_index][i].posted = ride_posted_time;
          attractions[park_index][i].id = ride_id;
          int j;
          for(j = 0; j < stored_watched_count; j++) {
            if(ride_id == stored_watched_ids[j] && watched_count < MAX_WATCHED_RIDES) {
              if(!attractions[park_index][i].watched)
                watched_count++;
              attractions[park_index][i].watched = 1;
            }
          }
          strncpy(attractions[park_index][i].name, ride_name_buffer, 20);
          strncpy(attractions[park_index][i].status, ride_status_buffer, 20);
          break;
        }
      }
    }

    break;
  }
  case KEY_TYPE_PARK_HOURS: {
    APP_LOG(APP_LOG_LEVEL_INFO, "Park %d update complete!", park_index);
    strncpy(parks[park_index].hours, park_hours_buffer, 10);
    parks[park_index].valid = true;
    if(++hours_update_count == num_parks) {
      hours_update_count = 0;
      parks_refresh();
    }

    break;
  }
  case KEY_TYPE_UPDATE_COMPLETE:
    APP_LOG(APP_LOG_LEVEL_INFO, "Update complete! Park %d.", park_index);
    if(++waits_update_count == num_parks) {
      last_update_time = time(NULL);
      waits_update_count = 0;      
      attractions_refresh();
      if(get_flagged_attractions_count()) {
        update_show();
      }
    }

    break;
  default:
    APP_LOG(APP_LOG_LEVEL_ERROR, "Key type %d not recognized!", type);
    break;
  }
}

static void inbox_dropped_callback(AppMessageResult reason, void *context) {
  APP_LOG(APP_LOG_LEVEL_ERROR, "Message dropped!");
}

static void outbox_failed_callback(DictionaryIterator *iterator, AppMessageResult reason, void *context) {
  APP_LOG(APP_LOG_LEVEL_ERROR, "Outbox send failed!");
}

static void outbox_sent_callback(DictionaryIterator *iterator, void *context) {
  APP_LOG(APP_LOG_LEVEL_INFO, "Outbox send success!");
}

static void init() {
  app_message_register_inbox_received(inbox_received_callback);
  app_message_register_inbox_dropped(inbox_dropped_callback);
  app_message_register_outbox_failed(outbox_failed_callback);
  app_message_register_outbox_sent(outbox_sent_callback);
  
  app_message_open(app_message_inbox_size_maximum(), app_message_outbox_size_maximum());
  
  data_init();
  attractions_init();
  update_init();
  parks_init();

  tick_timer_service_subscribe(SECOND_UNIT, tick_handler);
}

static void deinit() {
  parks_deinit();
  update_deinit();
  attractions_deinit();
  data_deinit();
}

int main(void) {
  init();
  app_event_loop();
  deinit();
}
