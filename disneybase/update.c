#include <pebble.h>
#include "update.h"
#include "constants.h"
#include "data.h"

static struct UpdateUi {
  Window *window;
  SimpleMenuLayer *list_layer;
  SimpleMenuItem list_items[MAX_WATCHED_RIDES];
  char list_status_buffers[MAX_WATCHED_RIDES][20];
  SimpleMenuSection list_section[1];
  bool visible;
  
  #ifdef PBL_SDK_3
    StatusBarLayer *status_bar;
  #endif
} ui;

static void select_callback(int index, void *ctx) {

}

static void window_load(Window *window) {
  //notification_dismissed = 0;
  ui.visible = true;
  int list_size = get_flagged_attractions_count();
  int cur_index = 0, park_index;
  
  for(park_index = 0; park_index < num_parks; park_index++) {
    int i, cap = get_attractions_count(park_index);
    for(i = 0; i < cap; i++) {
      if(attractions[park_index][i].flagged) {
        attractions[park_index][i].flagged = 0;
        if(attractions[park_index][i].posted == -1)
          snprintf(ui.list_status_buffers[cur_index], sizeof(ui.list_status_buffers[cur_index]), "%s", attractions[park_index][i].status);
        else
          snprintf(ui.list_status_buffers[cur_index], sizeof(ui.list_status_buffers[cur_index]), "%d min", attractions[park_index][i].posted);
        
        ui.list_items[cur_index] = (SimpleMenuItem) {
          .title = attractions[park_index][i].name,
          .subtitle = ui.list_status_buffers[cur_index],
          .callback = select_callback
        };
        cur_index++;
      }
    }
  }
  
  ui.list_section[0] = (SimpleMenuSection) {
    .num_items = list_size,
    .items = ui.list_items
  };
  
  Layer *window_layer = window_get_root_layer(window);
  GRect bounds = layer_get_frame(window_layer);
  #ifdef PBL_SDK_3
    bounds.origin.y += STATUS_BAR_LAYER_HEIGHT;
    bounds.size.h -= STATUS_BAR_LAYER_HEIGHT;
  #endif
  ui.list_layer = simple_menu_layer_create(bounds, window, ui.list_section, 1, NULL);
  
  #ifdef PBL_COLOR
    menu_layer_set_highlight_colors(simple_menu_layer_get_menu_layer(ui.list_layer), GColorFromHEX(color_hex), GColorBlack);
    menu_layer_set_normal_colors(simple_menu_layer_get_menu_layer(ui.list_layer), GColorWhite, GColorBlack);
  #endif
  
  // Add it to the window for display
  layer_add_child(window_layer, simple_menu_layer_get_layer(ui.list_layer));
  
  // Setup status bar
  #ifdef PBL_SDK_3
    ui.status_bar = status_bar_layer_create();
    status_bar_layer_set_colors(ui.status_bar, GColorFromHEX(color_hex), GColorBlack);
    layer_add_child(window_get_root_layer(window), status_bar_layer_get_layer(ui.status_bar));
  #endif
}

static void window_unload(Window *window) {
  //notification_dismissed = 1;
  ui.visible = false;
  simple_menu_layer_destroy(ui.list_layer);
  
  #ifdef PBL_SDK_3
    status_bar_layer_destroy(ui.status_bar);
  #endif
}
  
void update_init(void) {
  ui.visible = false;
  ui.window = window_create();
  
  window_set_window_handlers(ui.window, (WindowHandlers){
    .load = window_load,
    .unload = window_unload
  });
}

void update_deinit(void) {
  window_destroy(ui.window);
}

void update_show() {
  if(ui.visible)
    window_stack_pop(false);
  vibes_double_pulse();
  window_stack_push(ui.window, true);
}

