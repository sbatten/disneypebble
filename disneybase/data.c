#include <pebble.h>
#include "data.h"
#include "constants.h"
  
int get_attractions_count(int park_index) {
  int i;
  for(i = 0; i < MAX_ATTRACTIONS; i++) {
    if(attractions[park_index][i].id == -1)
      return i;
  }
  return MAX_ATTRACTIONS;
}
  
int get_flagged_attractions_count() {
  int park_index, i, count = 0;
  for(park_index = 0; park_index < num_parks; park_index++) {
    int cap = get_attractions_count(park_index);
    for(i = 0; i < cap; i++) {
      if(attractions[park_index][i].flagged) {
        count++;
      }
    }
  }
  return count;
}

void data_init(void) {
  last_update_time = 0;
  watched_count = 0;
  stored_watched_count = 0;
  int park_index, i;
  for(park_index = 0; park_index < num_parks; park_index++) {
    parks[park_index].valid = false;
    for(i = 0; i < MAX_ATTRACTIONS; i++) {
      attractions[park_index][i].id = -1;
      attractions[park_index][i].watched = 0;
      attractions[park_index][i].flagged = 0; 
    }
  }
  
  if(persist_exists(WATCHED_COUNT_KEY)) {
    stored_watched_count = persist_read_int(WATCHED_COUNT_KEY);
    persist_read_data(WATCHED_DATA_KEY, stored_watched_ids, sizeof(stored_watched_ids));
  }
}

void data_deinit(void) {
 int32_t watched_ids[MAX_WATCHED_RIDES];
  int i, j, cur_index = 0;
  for(i = 0; i < num_parks; i++) {
    for(j = 0; j < get_attractions_count(i); j++) {
      if(attractions[i][j].watched)
        watched_ids[cur_index++] = attractions[i][j].id;
    }
  }
  
  persist_write_int(WATCHED_COUNT_KEY, watched_count);
  persist_write_data(WATCHED_DATA_KEY, watched_ids, sizeof(watched_ids)); 
}

void request_new_data(void) {
    // Begin dictionary
    DictionaryIterator *iter;
    app_message_outbox_begin(&iter);
  
    // Add a key-value pair
    dict_write_uint8(iter, 0, 0);
  
    // Send the message!
    app_message_outbox_send();
}

