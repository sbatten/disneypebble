// Listen for when the watchface is opened
Pebble.addEventListener('ready', 
  function(e) {
    //console.log("PebbleKit JS ready!");
    //getWaitTimes();
  }
);

// Listen for when an AppMessage is received
Pebble.addEventListener('appmessage',
  function(e) {
    //console.log("AppMessage received!");
    var i;
    for(i = 0; i < Disney.Parks.length; i++) {
      getParkHours(i);
      //getWaitTimes(i);
    }
    
     for(i = 0; i < Disney.Parks.length; i++) {
      //getParkHours(i);
      getWaitTimes(i);
    }
  }                     
);

var xhrRequest = function(url, type, headers, params, callback) {
  var xhr = new XMLHttpRequest();
  
  xhr.onload = callback;
  xhr.open(type, url);
  
  var key;
  for(key in headers) {
    xhr.setRequestHeader(key, headers[key]);
  }
  xhr.send(params);
};

function DisneyApiRequest() {
  this.get = function(id, subpage, callback) {
    //console.log("DisneyApiRequest.get to " + id);
    if(typeof subpage == "function") {
      callback = subpage;
      subpage = "";
    }
    
    MakeGet("http://wdwaits.azurewebsites.net/" + subpage + "/" + id, callback);
  };

  function CheckAccessToken(callback) {
    callback();
  }

  /** Make a GET request to the Disney API */
  function MakeGet(url, callback) {
    CheckAccessToken(function(error) {
      if (error) {
        if (callback) callback(error);
        return;
      }

      xhrRequest(url, "GET", {}, {},
        function() {
          var responseText = this.responseText;
          //console.log(JSON.stringify(responseText, null, 2));
          if (error) {
            if (callback) callback(error);
            return;
          }

          // attempt to parse the body for JSON Data
          var JSONData = false;

          try {
            JSONData = JSON.parse(responseText);
          } catch (e) {
            if (callback) callback(false);
          }

          if (JSONData) {
            if (callback) callback(false, JSONData);
          }
        });
    });
  }
}


function Park(ParkID) {
  this.ParkID = ParkID;
  this.GetWaitTimes = function(callback) {
    //console.log("getting wait times for park: " + this.ParkID);
    var request = new DisneyApiRequest();
    request.get(this.ParkID, "waits", callback);
  };
  
  this.GetHours = function(callback) {
    //console.log("getting wait times for park: " + this.ParkID);
    var request = new DisneyApiRequest();
    request.get(this.ParkID, "hours", callback);
  };
}

var Disney = new DisneyApi();

function getParkHours(parkIndex) {
  Disney.Parks[parkIndex].GetHours(
    function(err, data) {
      if(err) {
        return;
      }
      
      var DateFormat = function(date) {
        var mm = "" + (date.getMonth() + 1);
        var dd = "" + date.getDate();
        var YYYY = "" + date.getFullYear();
        if(mm.length < 2)
          mm = "0" + mm;
        if(dd.length < 2)
          dd = "0" + dd;
        return YYYY + "-" + mm + "-" + dd;
      };
      
      var TimeFormat = function(time) {
        var tmp = new Date(time);
        var h = tmp.getHours();
        var m = tmp.getMinutes();
        var suffix = 'am';
        if(h >= 12) {
          h -= 12;
          suffix = 'pm';
        }
        
        if(h === 0)
          h = 12;
        return h + "" + (m === 0 ? "" : m) + "" + suffix;
      };
      
      var today = DateFormat(new Date());
      //console.log(JSON.stringify(data.schedules, null, 2));
      var parkSchedule, hours;
      for(var i = 0; i < data.length; i++) {
        parkSchedule = data[i];
        if(parkSchedule.date !== today)
          continue;
        
        if(parkSchedule.type === "Operating")
          
        hours = TimeFormat(parkSchedule.openingTime) + "-" + TimeFormat(parkSchedule.closingTime);
        //console.log(JSON.stringify(parkSchedule, null, 2));
      }
      
      function sendHours() {
        var dictionary = {
          "KEY_TYPE_PARK_HOURS" : 1,
          "KEY_PARK_INDEX" : parkIndex,
          "KEY_PARK_HOURS": hours
        };
        
        Pebble.sendAppMessage(dictionary, hoursSentSuccess, hoursSentFailure);
      }
      
      function hoursSentSuccess(e) {
        return;
      }
      
      function hoursSentFailure(e) {
        sendHours();
      }
      
      sendHours();
    }
  );
}

var map = {count:0};
function hashString(s) {
  if(map[s] === undefined) {
    map[s] = map.count++;
  }
  return map[s];
}

function getWaitTimes(parkIndex) {
  var Attractions = [];
  Disney.Parks[parkIndex].GetWaitTimes(
    function(err, data) {
      if(err) {
        //console.log(err);
        return;
      }
      
      //var transactions = [];
      var attraction;
      var curId = 0;
      
      function sendCurrentRide() {
        attraction = Attractions[curId];
        var id = hashString(attraction.id);
        var name = attraction.name;
        var status = attraction.status;
        var postedTime = attraction.waitTime;

        // Assemble dictionary using our keys
        var dictionary = {
          "KEY_TYPE_RIDE_INFO" : 1,
          "KEY_PARK_INDEX" : parkIndex,
          "KEY_RIDE_ID": id,
          "KEY_RIDE_NAME": name,
          "KEY_RIDE_STATUS": status,
          "KEY_RIDE_POSTED": postedTime
        };

        // Send to Pebble
        Pebble.sendAppMessage(dictionary, rideSentSuccess, rideSentFailure);
      }
      
      function sendComplete() {
        // Assemble dictionary using our keys
        var dictionary = {
          "KEY_TYPE_UPDATE_COMPLETE" : parkIndex
        };

        // Send to Pebble
        Pebble.sendAppMessage(dictionary, completeSuccess, completeFailure);
      }
      
      function completeSuccess(e) {
        //console.log("sent complete");
      }
      
      function completeFailure(e) {
        sendComplete();
      }
      
      function rideSentSuccess(e) {
        curId++;
        //console.log("sent " + Attractions[curId].name + " successfully");
        if(curId == Attractions.length) {
          sendComplete();
          return;
        }
        sendCurrentRide();
      }
      
      function rideSentFailure(e) {
        //console.log("failed to send ride info");
        sendCurrentRide();
      }

      
      for(var i = 0; i < data.length; i++) {
        attraction = data[i];
        
        if(attraction.waitTime === undefined)
          attraction.waitTime = -1;

        if(attraction.active)
          attraction.status = "Open";
        else {
          attraction.status = "Closed";
          attraction.waitTime = -1;
        }

        Attractions.push(attraction);
      }
      
      var status_priority = [];
      status_priority["Open"] = 1;
      status_priority["Closed Temporarily"] = 2;
      status_priority["Closed"] = 3;
      
      Attractions.sort(function(a, b) {
        
        var spa = status_priority[a.status];
        var spb = status_priority[b.status];
        if(spa === undefined) spa = 4;
        if(spb === undefined) spb = 4;
        
        var aHasWait = a.waitTime !== -1;
        var bHasWait = b.waitTime !== -1;
        
        if(spa === spb) {
          if(aHasWait === bHasWait) {
            return a.name.localeCompare(b.name);
          }
          
          if(aHasWait) return -1;
          return 1;
        }
        
        return spa - spb;
      });
      
      sendCurrentRide();
    });
}
