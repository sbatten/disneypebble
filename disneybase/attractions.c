#include <pebble.h>
#include "attractions.h"
#include "constants.h"
#include "data.h"

#define NUM_RIDE_MENU_SECTIONS 1
  
static struct AttractionsUi {
  Window *window;
  MenuLayer *list_layer;
  bool visible;
  
  #ifdef PBL_SDK_3
    StatusBarLayer *status_bar;
  #endif
} ui;

typedef struct ParkInfo {
  int index;
} ParkInfo;

static ParkInfo park_info;

// A callback is used to specify the amount of sections of menu items
// With this, you can dynamically add and remove sections
static uint16_t get_num_sections_callback(MenuLayer *menu_layer, void *data) {
  return NUM_RIDE_MENU_SECTIONS;
}

// Each section has a number of items;  we use a callback to specify this
// You can also dynamically add and remove items using this
static uint16_t get_num_rows_callback(MenuLayer *menu_layer, uint16_t section_index, void *data) {
  if(park_info.index == -1)
    return 0;
  int ret = get_attractions_count(park_info.index);
  return ret > 0 ? ret : 1;
}

// A callback is used to specify the height of the section header
static int16_t get_header_height_callback(MenuLayer *menu_layer, uint16_t section_index, void *data) {
  // This is a define provided in pebble.h that you may use for the default height
  return MENU_CELL_BASIC_HEADER_HEIGHT;
}

// Here we draw what each header is
static void draw_header_callback(GContext* ctx, const Layer *cell_layer, uint16_t section_index, void *data) {
  // Draw title text in the section header
  static char header_buffer[30];
  time_t now = time(NULL);

  #ifdef PBL_PLATFORM_CHALK
    static char header_indent[10] = "         ";
  #else
    static char header_indent[10] = "";
  #endif
  
  if(last_update_time == 0)
    snprintf(header_buffer, sizeof(header_buffer), "%s%s", header_indent, park_names[park_info.index]);
  else {
    snprintf(header_buffer, sizeof(header_buffer), "%s%dm ago | %s", header_indent, ((int)difftime(now, last_update_time)/60), park_names[park_info.index]);
  }
  
  menu_cell_basic_header_draw(ctx, cell_layer, header_buffer);
}

// This is the menu item draw callback where you specify what each item should look like
static void draw_row_callback(GContext* ctx, const Layer *cell_layer, MenuIndex *cell_index, void *data) {
  static char status_buffer[20];
  static char name_buffer[25];
  // Use the row to specify which item we'll draw
  int row = cell_index->row + 1;
  
  if(get_attractions_count(park_info.index) == 0) {
    menu_cell_basic_draw(ctx, cell_layer, "No ride information", "Loading...", NULL);
    return;
  }
  
  
  int i, count = 0;
  for(i = 0; i < MAX_ATTRACTIONS; i++) {
    if(attractions[park_info.index][i].id != -1)
      count++;
    if(count == row) {
      if(attractions[park_info.index][i].posted == -1)
        snprintf(status_buffer, sizeof(status_buffer), "%s", attractions[park_info.index][i].status);
      else
        snprintf(status_buffer, sizeof(status_buffer), "%d min", attractions[park_info.index][i].posted);
      
      // Prepare name for display
      snprintf(name_buffer, sizeof(name_buffer), "%s%s", (attractions[park_info.index][i].watched ? "*" : ""), attractions[park_info.index][i].name);
      
      menu_cell_basic_draw(ctx, cell_layer, name_buffer, status_buffer, NULL);
      break;
    }
  }
}

// Here we capture when a user selects a menu item
void select_callback(MenuLayer *menu_layer, MenuIndex *cell_index, void *data) {
  if(get_attractions_count(park_info.index) == 0)
    return;
  
  if(attractions[park_info.index][cell_index->row].watched) {
    watched_count--;
    attractions[park_info.index][cell_index->row].watched = 0;
  } else if(watched_count < MAX_WATCHED_RIDES) {
    watched_count++;
    attractions[park_info.index][cell_index->row].watched = 1;
  }
  
  layer_mark_dirty(menu_layer_get_layer(ui.list_layer));
}

void long_select_callback(MenuLayer *menu_layer, MenuIndex *cell_index, void *data) {
  vibes_short_pulse();
  request_new_data();
}

static void window_load(Window *window) {
  ui.visible = true;
  
  GRect menu_frame = layer_get_frame(window_get_root_layer(window));
  #ifdef PBL_SDK_3
    menu_frame.origin.y += STATUS_BAR_LAYER_HEIGHT;
    menu_frame.size.h -= STATUS_BAR_LAYER_HEIGHT;
  #endif
  ui.list_layer = menu_layer_create(menu_frame);
  
  #ifndef PBL_BW
    menu_layer_set_highlight_colors(ui.list_layer, GColorFromHEX(color_hex), GColorBlack);
    menu_layer_set_normal_colors(ui.list_layer, GColorWhite, GColorBlack);
    menu_layer_set_center_focused(ui.list_layer, false);
  #endif
  
  // Set all the callbacks for the menu layer
  menu_layer_set_callbacks(ui.list_layer, NULL, (MenuLayerCallbacks){
    .get_num_sections = get_num_sections_callback,
    .get_num_rows = get_num_rows_callback,
    .get_header_height = get_header_height_callback,
    .draw_header = draw_header_callback,
    .draw_row = draw_row_callback,
    .select_click = select_callback,
    .select_long_click = long_select_callback
  });
  
  // Bind the menu layer's click config provider to the window for interactivity
  menu_layer_set_click_config_onto_window(ui.list_layer, window);
  
  // Add it to the window for display
  layer_add_child(window_get_root_layer(window), menu_layer_get_layer(ui.list_layer));
  
  // Setup status bar
  #ifdef PBL_SDK_3
    ui.status_bar = status_bar_layer_create();
    status_bar_layer_set_colors(ui.status_bar, GColorFromHEX(color_hex), GColorBlack);
    layer_add_child(window_get_root_layer(window), status_bar_layer_get_layer(ui.status_bar));
  #endif
}

static void window_unload(Window *window) {
  ui.visible = false;
  menu_layer_destroy(ui.list_layer);
  ui.list_layer = NULL;
  
  #ifdef PBL_SDK_3
    status_bar_layer_destroy(ui.status_bar);
  #endif
}

void attractions_show(int park_index) {
    if(ui.visible)
      return;
    park_info.index = park_index;
    window_stack_push(ui.window, true);
}

void attractions_refresh(void) {
  if(ui.visible) {
    menu_layer_reload_data(ui.list_layer);
    layer_mark_dirty(menu_layer_get_layer(ui.list_layer));
  }
}

void attractions_init(void) {
  ui.visible = false;
  park_info.index = -1;
  ui.window = window_create();
  
  window_set_window_handlers(ui.window, (WindowHandlers){
    .load = window_load,
    .unload = window_unload
  });
}

void attractions_deinit(void) {
  window_destroy(ui.window);
}

