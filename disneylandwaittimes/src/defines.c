#include "../../disneybase/constants.h"
#include <pebble.h>

const int color_hex = 0x55AAFF;

const int num_parks = 2;
char park_names[MAX_NUM_PARKS][40] = {"Disneyland", "California Adventure", "", ""};

int park_icon_ids[MAX_NUM_PARKS] = { RESOURCE_ID_ICON_DL, RESOURCE_ID_ICON_DCA, 0, 0 };
