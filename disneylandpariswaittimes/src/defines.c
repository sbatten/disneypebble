#include "../../disneybase/constants.h"
#include <pebble.h>

const int color_hex = 0xAA00FF;

const int num_parks = 2;
char park_names[MAX_NUM_PARKS][40] = {"Disneyland Paris", "Walt Disney Studios", "", ""};

int park_icon_ids[MAX_NUM_PARKS] = { RESOURCE_ID_ICON_DLP, RESOURCE_ID_ICON_WDS, 0, 0 };
